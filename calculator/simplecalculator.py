

class Calculator:
	def __init__(self, a, b):
		self.a = a
		self.b = b

	def add(self):
		print("-----------------Addtion ------------------")
		return self.a + self.b

	def mul(self):
		print("----------------- Multiplication ------------------")
		return self.a * self.b

	def div(self):
		print("----------------- Division ------------------")
		return self.a / self.b

	def sub(self):
		print("----------------- Subtraction ------------------")
		return self.a - self.b

	def add2(self, a, b):
		print("------------addintion---2 ----------")
		return a + b


if __name__ == "__main__":
	a = int(input("Enter first number : "))
	b = int(input("Enter second number :"))
	cal = Calculator(a, b)

	choice = 1	

	while choice != 0:

		print("1 Add")
		print("2 Multiply")
		print("3 Division")
		print("4 Subtraction")
		print("0 Existing")

		choice = int(input("Enter your choice : "))
		if choice == 1:
			print("Result : ", cal.add())
		elif choice == 2:
			print("Result : ", cal.mul())
		elif choice == 3:
			print("Result : ", cal.div())
		elif choice == 4:
			print("Result : ", cal.sub())
		elif choice == 0:
			print("Existing!")
		else:
			print("Invalid choice!")
