
import unittest
import sys
sys.path.append("./")

from calculator.simplecalculator import Calculator

class TestSimpleCal(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		print("In setupclass method")
		cls.cal = Calculator(4, 5)

	@classmethod
	def tearDownClass(cls):
		print("In tearDownClass method")
		del cls.cal

	def setUp(self):
		print("In setup method")
		self.cal.a = 5
		self.cal.b = 8

	def tearDown(self):
		print("In tearDown method!")
		self.cal.a = 0
		self.cal.b = 0

	def test_simpleadd(self):
		print("simle add test call")
		add = self.cal.add()
		self.assertAlmostEqual(10, add, delta=3)

	def test_simplesub(self):
		sub = self.cal.sub()
		self.assertGreater(3, sub)

	def test_simplesubFail(self):
		sub = self.cal.sub()
		self.assertNotEqual(6, sub)

	def test_assertIs_multiply(self):
		self.cal.a = 4
		self.cal.b = 1.2
		mul = self.cal.mul()
		self.assertIs(type(1.2), type(mul))

	def test_division(self):
		self.cal.a = 12
		self.cal.b = 4
		div = self.cal.div()
		self.assertEqual(3, div)

	def test_zerodivision(self):
		self.cal.a = 11
		self.cal.b = 0
		self.assertRaises("ZeroDivisionError", self.cal.div())
		with self.assertRaises(TypeError):
			self.cal1 = Calculator()


if __name__ == "__main__":
	unittest.main()